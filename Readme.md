## Mini Moneyme Coding Challenge

A Demo project to mimic moneyme's online personal loan quote process with React, dotnet core 2.2 and postgresSQL.

### Overview
This project uses react as the client side single page application and dotnet core as the backend API which connects to postgresSQL database. 

### Front-end design

Simple react web application. For simplicity, UI polishing, state management using hooks or redux is outside of this scope. It is worth mentioning a simple but good form validation is in place.

### Backend design

The functional requirement is simply to display a web form, calculate repayment and persist data on the server. But I gave some deeper thinking on backend architecture design using domain driven design and onion architecture.

![Onion Architecture](onion.png "Architecture")

As illustrated by the graph, I defined a Quote Aggregate root as the unit for the purpose of data changes. Any request will go to the service layer which manipulates the domain object by reading/changing its state, after that the aggregate root is transferred to the infrastructure layer where it is mapped to corresponding database tables.
 
### Features
- API Link Generation
- Web page for opening, editing, reviewing quote, repayment and apply for the loan
- database to save quote and application status
- basic form validation using React formik

### Database Schema

![Onion Architecture](db.schema.png "db schema")

A loan user table is to store user info, and loan quote is used to store every quote request sent from the client. The quote and user is a many-to-one relationship.

Given a user is very likely to check and apply multiple times, it would make sense to link all these quotes with that particular user. In this project, every time when a quote is requested, it will check the email or mobile. If the same email or mobile exists in the user table. The quote will be linked to that existing user.

### Setup

#### Local setup

Install Requirements: 
- node.js
- dotnet sdk 2.2
- postgres sql
- create-react-app

Restore dotnet dependencies:

```bash
  dotnet restore
```

Run Entity framework migration:

```bash
cd src/MiniMoneyMe.Infrastructure
dotnet ef database update
cd ..
```

Install and run react web project:

```bash
cd src/MiniMoneyMe.Web
npm install
npm start
cd ..
```

Run .net project 

```bash 
dotnet start 
```

#### Docker setup

If docker is installed, you can skip the above process and just run to run project.
```
docker-compose up
```

Once finished, run:

```
docker-compose down
```

### Test

After local setup, run 

```
dotnet test
```

This will run the unit test under test/ folder.

### Assumptions and Limitation

- Database schema might need one more table to store loan application.
- Users' email will be updated by the newer quote if they use a different email but the same phone number and vice versa.
- for simplicity, no front end and repository layer test were written.
