FROM microsoft/dotnet:2.2-sdk
WORKDIR /app
COPY ["src/MiniMoneyMe.Domain/MiniMoneyMe.Domain.csproj", "src/MiniMoneyMe.Domain/"]
COPY ["src/MiniMoneyMe.Service/MiniMoneyMe.Service.csproj", "src/MiniMoneyMe.Service/"]
COPY ["src/MiniMoneyMe.Infrastructure/MiniMoneyMe.Infrastructure.csproj", "src/MiniMoneyMe.Infrastructure/"]
COPY ["src/MiniMoneyMe.Api/MiniMoneyMe.Api.csproj", "src/MiniMoneyMe.Api/"]
COPY ["test/MiniMoneyMe.Service.Test/MiniMoneyMe.Service.Test.csproj", "test/MiniMoneyMe.Service.Test/"]
COPY ["MiniMoneyme.sln", "./"]

RUN dotnet restore "MiniMoneyme.sln"

COPY . .
WORKDIR /app
RUN dotnet build "src/MiniMoneyMe.Api/MiniMoneyMe.Api.csproj" -c Release -o /build
RUN chmod +x ./seedAndRun.sh