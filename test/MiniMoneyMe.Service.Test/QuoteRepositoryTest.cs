using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MiniMoneyMe.Domain.QuoteAggregate;
using MiniMoneyMe.Infrastructure.Common;
using MiniMoneyMe.Infrastructure.Model;
using MiniMoneyMe.Infrastructure.Repository;
using MiniMoneyMe.Service.Dto;
using MiniMoneyMe.Service.QuoteService;
using NSubstitute;

namespace MiniMoneyMe.Infrastructure.Test
{
    [TestClass]
    public class QuoteServiceTest
    {
        private IQuoteService _quoteService;
        private IQuoteRepository _quoteRepositoryMock;

        [TestInitialize]
        public void Init()
        {
            Reset();
        }

        private void Reset()
        {
            _quoteRepositoryMock = Substitute.For<IQuoteRepository>();
            _quoteService = new QuoteService(_quoteRepositoryMock);
        }

        [TestMethod]
        public async Task Should_Create_A_New_Quote_ForNew_Request()
        {
            Reset();

            var quoteDto = new QuoteRequestDto
            {
                FirstName = "John",
                LastName = "Watson",
                Email = "j.h@doctor.com",
                Mobile = "0414223112",
                Amount = 10000,
                Term = 10,
                InterestRate = 10
            };

            await _quoteService.CreateOrUpdateQuote(quoteDto);

            await _quoteRepositoryMock.Received(1)
                .Save(Arg.Is<Quote>(q =>
                    q.FirstName == quoteDto.FirstName &&
                    q.LastName == quoteDto.LastName &&
                    q.Amount.Equals(quoteDto.Amount) &&
                    q.Term.Equals(quoteDto.Term) &&
                    q.InterestRate.Equals(quoteDto.InterestRate)));
        }

        [TestMethod]
        public async Task Should_Apply_A_Quote()
        {
            Reset();

            var quote = new Quote("123456", "John",
                "Watson", "j.h@doctor.com", "0414223112",
                10000, 10, 10);

            _quoteRepositoryMock.GetById("123456").Returns(quote);

            await _quoteService.ApplyQuote("123456");

            await _quoteRepositoryMock.Received(1)
                .Save(Arg.Is<Quote>(q =>
                    q.FirstName == quote.FirstName &&
                    q.LastName == quote.LastName &&
                    q.Amount.Equals(quote.Amount) &&
                    q.Term.Equals(quote.Term) &&
                    q.InterestRate.Equals(quote.InterestRate) &&
                    quote.HasApplied));
        }
    }
}