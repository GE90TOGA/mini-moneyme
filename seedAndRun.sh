#!/bin/bash

cd src/MiniMoneyMe.Infrastructure 

dotnet ef database update

cd /build
dotnet MiniMoneyMe.Api.dll