import * as React from 'react';
import QuoteService from '../service/quoteService';

class QuoteReview extends React.Component {
  state = {
    quote: null
  };

  componentDidMount = async () => {
    const { match } = this.props;
    try {
      const quote = await QuoteService.getQuoteById(match.params.id);
      this.setState({
        quote
      });
    } catch (err) {
      alert(err);
      this.props.history.replace('/');
    }
  };

  applyLoan = async () => {
    try {
      await QuoteService.applyLoan(this.state.quote.id);
      this.setState({
        quote: {
          ...this.state.quote,
          hasApplied: true
        }
      });
    } catch (err) {
      alert(err);
    }
  };

  render() {
    if (this.state.quote) {
      const { quote } = this.state;
      return (
        <div className="container" style={{ width: '50%' }}>
          <p className="font-weight-bold">Your Information:</p>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <p>Name</p>
            <p>
              {quote.firstName} {quote.lastName}
            </p>
          </div>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <p>Mobile</p>
            <p>{quote.mobile}</p>
          </div>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <p>Email</p>
            <p>{quote.email}</p>
          </div>

          <p className="font-weight-bold">Finance Details:</p>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <p>Finance Amount:</p>
            <p>
              ${quote.amount} over {quote.term} months
            </p>
          </div>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <p>Repayments:</p>
            <p>{parseFloat(quote.repayment.toFixed(2))} per month</p>
          </div>

          <div style={{ display: 'flex', justifyContent: 'center' }}>
            {quote.hasApplied ? (
              <p>Your have applied, we are reviewing it and will get back to you soon :)</p>
            ) : (
              <button type="submit" className="btn btn-success" onClick={this.applyLoan}>
                Apply Now
              </button>
            )}
          </div>
        </div>
      );
    }

    return <h2>Loading ...</h2>;
  }
}

export default QuoteReview;
