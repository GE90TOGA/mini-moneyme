import React, { Component } from 'react';
import QuoteService from '../service/quoteService';
import InputForm from '../components/InputForm/InputForm';

class CalculateQuote extends Component {
  state = {
    loading: true,
    quote: null
  };

  componentDidMount = async () => {
    const { match } = this.props;
    try {
      const quote = await QuoteService.getQuoteById(match.params.id);
      if(quote.hasApplied){
        this.props.history.replace(`/quotes/${quote.id}/review`);
        return;
      }
      this.setState({ quote }, () => {
        this.setState({ loading: false });
      });
    } catch (err) {
      alert(err);
      this.props.history.replace('/');
    }
  };

  calculateQuote = async (values, setSubmitting) => {
    await QuoteService.updateQuote({ id: this.state.quote.id, ...values });
    setSubmitting(false);
    this.props.history.push(`${this.state.quote.id}/review`, { quoteId: this.state.quote.id });
  };

  render() {
    return (
      <div>
        {this.state.loading ? (
          <h1>Loading...</h1>
        ) : (
          <InputForm onSubmit={this.calculateQuote} {...this.state.quote} />
        )}
      </div>
    );
  }
}

export default CalculateQuote;
