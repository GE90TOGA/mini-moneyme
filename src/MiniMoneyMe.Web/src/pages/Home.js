import React, { Component } from 'react';
import { config } from '../config';
class Home extends Component {
  render() {
    return (
      <div className="container">
        <h1>Home</h1>

        <p>
          <strong>
            This is Mini Moneyme, you can use this small demo to explore how online personal loan
            works.
          </strong>
        </p>

        <h3>Steps:</h3>
        <p>
          1. You need to call api to generate a invite link, you can do so by calling the api, for
          instance
        </p>
        <pre>
          {`curl -X POST ${config.serverUrl}/quotes \
  -H 'Accept: */*' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:5000' \
  -H 'Postman-Token: 01688d2e-4d1a-4ba1-bbba-663a61c67a3c,bafde2a2-955d-42be-aefc-301ccbf01a41' \
  -H 'User-Agent: PostmanRuntime/7.11.0' \
  -H 'accept-encoding: gzip, deflate' \
  -H 'cache-control: no-cache' \
  -H 'content-length: 165' \
  -d '{
	"firstName": "Mycroft",
	"lastName": "Holmes",
	"email": "mcc@detective.com",
	"mobile": "046111133441",
	"amount": "2100",
	"Term": "10",
	"interestRate": "10"
}'`}
        </pre>
        <p>2. Copy the returned invite link and open your browser with it</p>
        <p>3. Adjust conditions if you like.</p>
        <p>4. Review and apply</p>
        <p>Happy Coding !</p>
      </div>
    );
  }
}

export default Home;
