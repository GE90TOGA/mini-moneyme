import React, { Component } from 'react';

class Nomatch extends Component {
  componentDidMount() {
    this.props.history.replace('/home');
  }
  render() {
    return <></>;
  }
}

export default Nomatch;
