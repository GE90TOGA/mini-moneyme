import * as React from 'react';
import Slider from 'react-input-slider';
import './InputForm.css';
import { Formik } from 'formik';

class InputForm extends React.Component {
  state = {
    amount: 2100,
    term: 3,
    interestRate: 8.99
  };

  constructor(props) {
    super(props);
    const { amount, interestRate, term } = this.props;

    this.state = {
      amount,
      term,
      interestRate
    };
  }

  handleChangeAmount = ({ x }) => {
    this.setState({ amount: x });
  };

  handleChangeTerm = ({ x }) => {
    this.setState({ term: x });
  };

  handleChangeInterest = ({ x }) => {
    this.setState({ interestRate: parseFloat(x.toFixed(2)) });
  };

  render() {
    return (
      <div className="container">
        <Formik
          initialValues={{
            email: this.props.email,
            mobile: this.props.mobile,
            firstName: this.props.firstName,
            lastName: this.props.lastName
          }}
          validate={values => {
            // Can Use Yup like validation, but stick to the old style validation now
            let errors = {};

            if (!values.email) {
              errors.email = 'Email is Required';
            } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
              errors.email = 'Invalid email address';
            }

            if (!values.mobile) {
              errors.mobile = 'Mobile is Required';
            } else if (!/^04\d{8}$/i.test(values.mobile)) {
              errors.mobile = 'Invalid mobile number, should start with 04';
            }

            if (!values.firstName) {
              errors.firstName = 'First name is required';
            }

            if (!values.lastName) {
              errors.lastName = 'Last name is required';
            }

            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            this.props.onSubmit({ ...this.state, ...values }, setSubmitting);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting
            /* and other goodies */
          }) => (
            <form onSubmit={handleSubmit}>
              <div className="row">
                <div className="input-row col-md-6">
                  <input
                    type="text"
                    className={
                      errors.firstName && touched.firstName && errors.firstName
                        ? 'is-invalid form-control'
                        : 'form-control'
                    }
                    name="firstName"
                    placeholder="First name"
                    onChange={handleChange}
                    value={values.firstName}
                  />
                  {errors.firstName && touched.firstName && errors.firstName}
                </div>
                <div className="input-row col-md-6">
                  <input
                    type="text"
                    className={
                      errors.lastName && touched.lastName && errors.lastName
                        ? 'is-invalid form-control'
                        : 'form-control'
                    }
                    name="lastName"
                    placeholder="Last name"
                    onChange={handleChange}
                    value={values.lastName}
                  />
                  {errors.lastName && touched.lastName && errors.lastName}
                </div>
              </div>
              <div className="row">
                <div className="input-row  col-md-6">
                  <input
                    type="text"
                    className={
                      errors.mobile && touched.mobile && errors.mobile
                        ? 'is-invalid form-control'
                        : 'form-control'
                    }
                    name="mobile"
                    placeholder="Mobile Number"
                    onChange={handleChange}
                    value={values.mobile}
                  />
                  {errors.mobile && touched.mobile && errors.mobile}
                </div>
                <div className="input-row col-md-6">
                  <input
                    type="email"
                    name="email"
                    className={
                      errors.email && touched.email && errors.email
                        ? 'is-invalid form-control'
                        : 'form-control'
                    }
                    placeholder="Your Email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                  />
                  {errors.email && touched.email && errors.email}
                </div>
              </div>

              <div className="row">
                <div className="col-md-6">
                  <div>
                    <div>Amount:</div>
                    <div className="slider-input">
                      <Slider
                        axis="x"
                        xstep={1}
                        xmin={2100}
                        xmax={15000}
                        x={this.state.amount}
                        onChange={this.handleChangeAmount}
                      />
                    </div>

                    <div>{this.state.amount}</div>
                  </div>
                </div>

                <div className="col-md-6">
                  <div>
                    <div>Term (number of month to pay off):</div>
                    <div className="slider-input">
                      <Slider
                        axis="x"
                        xstep={1}
                        xmin={3}
                        xmax={36}
                        x={this.state.term}
                        onChange={this.handleChangeTerm}
                      />
                    </div>

                    <div>{this.state.term}</div>

                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-md-6">
                  <div>
                    <div>Interest Rate</div>
                    <div className="slider-input">
                      <Slider
                        axis="x"
                        xstep={0.01}
                        xmin={8.99}
                        xmax={30}
                        x={this.state.interestRate}
                        onChange={this.handleChangeInterest}
                      />
                    </div>
                    <div>{this.state.interestRate}</div>
                  </div>
                </div>
              </div>
              <div style={{ textAlign: 'center' }}>
                <button type="submit" className="btn btn-success" disabled={isSubmitting}>
                  Calculate Quote
                </button>
              </div>
            </form>
          )}
        </Formik>
      </div>
    );
  }
}

export default InputForm;
