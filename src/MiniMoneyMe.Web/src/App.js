import * as React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import CalculateQuote from './pages/CalculateQuote';
import QuoteReview from './pages/QuoteReview';
import Home from './pages/Home';
import Nomatch from './pages/Nomatch';

class App extends React.Component {
  render() {
    return (
      <Router>
        <nav className="navbar navbar-dark bg-dark">
          <Link to="home">
            <span className="navbar-brand mb-0 h1">Hero Form</span>
          </Link>
        </nav>

        <Switch>
          <Route path="/quotes/:id" component={CalculateQuote} exact={true} />
          <Route path="/quotes/:id/review" component={QuoteReview} exact={true} />
          <Route path="/home" component={Home} />
          <Route path="*" component={Nomatch} />
        </Switch>
      </Router>
    );
  }
}

export default App;
