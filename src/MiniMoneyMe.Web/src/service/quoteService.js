import { config } from '../config';
import axios from 'axios';

export default class QuoteService {
  static async getQuoteById(quoteId) {
    const response = await axios.get(`${config.serverUrl}/quotes/${quoteId}`);
    return response.data;
  }

  static async updateQuote(quote) {
    const response = await axios.put(`${config.serverUrl}/quotes`, quote);
    return response.data;
  }

  static async applyLoan(quoteId) {
    const response = await axios.post(`${config.serverUrl}/quotes/${quoteId}/application`);
    return response.data;
  }
}
