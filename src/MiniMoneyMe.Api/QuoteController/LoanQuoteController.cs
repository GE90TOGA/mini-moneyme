﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MiniMoneyMe.Service.Dto;
using MiniMoneyMe.Service.QuoteService;

namespace MiniMoneyMe.Api.QuoteController
{
    [Route("/api/quotes")]
    public class QuoteController : Controller
    {
        private readonly IQuoteService _quoteService;

        public QuoteController(IQuoteService quoteService)
        {
            _quoteService = quoteService;
        }


        [HttpGet]
        [Route("{quoteId}")]
        public async Task<IActionResult> GetQuote([FromRoute] string quoteId)
        {
            var quoteResponse = await _quoteService.GetQuoteById(quoteId);
            return Ok(quoteResponse);
        }

        // return the url
        [HttpPost]
        [Route("{quoteId}/application")]
        public async Task<IActionResult> ApplyQuote([FromRoute] string quoteId)
        {
            await _quoteService.ApplyQuote(quoteId);
            return Ok();
        }


        [HttpPut]
        public async Task<IActionResult> CreateOrUpdateQuote([FromBody] QuoteRequestDto quoteRequest)
        {
            var quote = await _quoteService.CreateOrUpdateQuote(quoteRequest);
            return Ok(quote);
        }

        // return the url
        [HttpPost]
        public async Task<IActionResult> CreateQuote([FromBody] QuoteRequestDto quoteRequest)
        {
            var quote = await _quoteService.CreateOrUpdateQuote(quoteRequest);
            // TODO: ADD URL link

            var link = $"http://localhost:3000/quotes/{quote.Id}";
            return Ok(new
            {
                Link = link
            });
        }
    }
}