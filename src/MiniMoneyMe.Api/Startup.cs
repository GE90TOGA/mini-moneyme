﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MiniMoneyMe.Api.Common;
using MiniMoneyMe.Infrastructure;
using MiniMoneyMe.Infrastructure.Common;
using MiniMoneyMe.Service;

namespace MiniMoneyMe.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));
            
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("MyPolicy"));
            });
            
            services.Configure<DbSetting>(options =>
            {
                var envConnectionString = Environment.GetEnvironmentVariable("POSTGRES_CONNECTIONSTRING");
                options.ConnectionString = !string.IsNullOrWhiteSpace(envConnectionString)
                    ? envConnectionString
                    : Configuration.GetSection("Postgres:ConnectionString").Value;
            });

            
            services.RegisterApplicationService();
            services.RegisterInfrastructure();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // app.UseHttpsRedirection();
            app.UseMiddleware<ErrorHandler>();
            app.UseCors("MyPolicy");
            app.UseMvc();
        }
    }
}