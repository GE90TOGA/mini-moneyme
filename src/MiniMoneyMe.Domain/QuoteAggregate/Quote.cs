using MiniMoneyMe.Domain.Common;
using MiniMoneyMe.Domain.Exceptions;
using MiniMoneyMe.Domain.Utils;

namespace MiniMoneyMe.Domain.QuoteAggregate
{
    public class Quote : AggregateRoot
    {
        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public string Email { get; private set; }

        public string Mobile { get; private set; }

        public double Amount { get; private set; }

        public int Term { get; private set; }

        public double InterestRate { get; private set; }

        public double RePayment { get; private set; }

        public bool HasApplied { get; private set; }


        public Quote(string id, string firstName, string lastName,
            string email, string mobile, double amount, int term,
            double interestRate, bool hasApplied) : this(id, firstName, lastName, email, mobile, amount, term,
            interestRate)
        {
            HasApplied = hasApplied;
        }

        public Quote(string id, string firstName, string lastName,
            string email, string mobile, double amount, int term,
            double interestRate) : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Mobile = mobile;

            if (amount <= 0)
            {
                throw new DomainValidationException("amount cannot be 0 or negative");
            }

            if (term <= 0)
            {
                throw new DomainValidationException("term cannot be 0 or negative");
            }

            // Ensure interest rate is at certain value by look up moneyme.com.au
            if (interestRate.Equals(null) || interestRate < 8.99)
            {
                interestRate = 8.99;
            }

            Amount = amount;
            Term = term;
            InterestRate = interestRate;
            CalculateRepayment();
        }

        public void ApplyQuote()
        {
            HasApplied = true;
        }


        private double CalculateRepayment()
        {
            RePayment = PMTUtil.PMT(InterestRate, Term, Amount);
            return RePayment;
        }
    }
}