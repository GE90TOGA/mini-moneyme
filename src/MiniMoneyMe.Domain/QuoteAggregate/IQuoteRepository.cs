using MiniMoneyMe.Domain.Common;

namespace MiniMoneyMe.Domain.QuoteAggregate
{
    public interface IQuoteRepository: IRepository<Quote>
    {
        
    }
}