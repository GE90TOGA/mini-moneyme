using System;

namespace MiniMoneyMe.Domain.Exceptions
{
    public class InvalidInputException : Exception
    {
        public InvalidInputException(string msg) : base(msg)
        {
        }
    }
}