using System;

namespace MiniMoneyMe.Domain.Exceptions
{
    public class ResourceNotFoundException : Exception
    {
        public ResourceNotFoundException(string msg) : base(msg)
        {
        }
    }
}