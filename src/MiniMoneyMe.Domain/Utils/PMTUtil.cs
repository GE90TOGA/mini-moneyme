using System;

namespace MiniMoneyMe.Domain.Utils
{
    public class PMTUtil
    {
        public static double PMT(double interestRate, int totalNumberOfMonths, double loanAmount)
        {
            var rate = (double) interestRate / 100 / 12;
            var denominator = Math.Pow((1 + rate), totalNumberOfMonths) - 1;
            return (rate + (rate / denominator)) * loanAmount;
        }
    }
}