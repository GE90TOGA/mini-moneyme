using System.Collections;
using System.Threading.Tasks;

namespace MiniMoneyMe.Domain.Common
{
    public interface IRepository<T> where T : AggregateRoot
    {
        Task<string> Save(T aggregate);

        Task<T> GetById(string id);
    }
}