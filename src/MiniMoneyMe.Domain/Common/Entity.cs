namespace MiniMoneyMe.Domain.Common
{
    public class Entity
    {
        public string Id { get; }

        public Entity(string id)
        {
            Id = id;
        }
    }
}