using System.ComponentModel.DataAnnotations;
using MiniMoneyMe.Infrastructure.Model.Common;

namespace MiniMoneyMe.Infrastructure.Model
{
    public class LoanQuoteModel : EntityModelBase
    {
        [Required] public decimal Amount { get; set; }

        [Required] public int Term { get; set; }

        [Required] public decimal InterestRate { get; set; }

        [Required] public string LoanUserId { get; set; } // reference LoadUserModel.Id

        [Required] public bool HasApplied { get; set; }

        public LoanUserModel LoanUser { get; set; }
    }
}