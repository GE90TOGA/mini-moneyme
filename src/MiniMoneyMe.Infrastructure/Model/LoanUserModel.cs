using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MiniMoneyMe.Infrastructure.Model.Common;

namespace MiniMoneyMe.Infrastructure.Model
{
    public class LoanUserModel : EntityModelBase
    {
        [Required] public string FirstName { get; set; }

        [Required] public string LastName { get; set; }

        [Required] public string Mobile { get; set; }

        [Required] public string Email { get; set; }

        public List<LoanQuoteModel> Quotes { get; set; }
    }
}