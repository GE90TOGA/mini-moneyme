using System.Threading.Tasks;
using MiniMoneyMe.Domain.Common;

namespace MiniMoneyMe.Infrastructure.Common
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : AggregateRoot
    {
        protected readonly MiniMoneyMeContext _dbContext;

        protected RepositoryBase(MiniMoneyMeContext dbContext)
        {
            _dbContext = dbContext;
        }

        public abstract Task<string> Save(T aggregate);

        public abstract Task<T> GetById(string id);
    }
}