using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using MiniMoneyMe.Infrastructure.Model;

namespace MiniMoneyMe.Infrastructure.Common
{
    public static class SeedDataService
    {
        public static void SeedData(ModelBuilder moduleBuilder)
        {
            var loanUsers = new List<LoanUserModel>
            {
                new LoanUserModel
                {
                    Id = "180c5352-2a8b-4941-9728-2790ab5e15af",
                    FirstName = "John",
                    LastName = "Doe",
                    Mobile = "0414331228",
                    Email = "johndoeloan@123.com"
                },
                new LoanUserModel
                {
                    Id = "591c586e-5d85-4fa0-afd3-f90bbb531895",
                    FirstName = "Steve",
                    LastName = "Jobs",
                    Mobile = "0411332112",
                    Email = "stevejobs@apple.ghost.com"
                }
            };

            moduleBuilder.Entity<LoanUserModel>().HasData(loanUsers);

            var quotes = new List<LoanQuoteModel>
            {
                new LoanQuoteModel
                {
                    Id = "ca24c626-873b-4d27-8e30-bfd26e760b13",
                    Amount = 3000,
                    Term = 3,
                    InterestRate = new Decimal(3.2),
                    LoanUserId = "180c5352-2a8b-4941-9728-2790ab5e15af",
                    HasApplied = false
                },
                new LoanQuoteModel
                {
                    Id = "2595e25f-4c80-4dba-9bac-72b554934157",
                    Amount = 10000,
                    Term = 4,
                    InterestRate = new Decimal(7.2),
                    LoanUserId = "180c5352-2a8b-4941-9728-2790ab5e15af",
                    HasApplied = true
                },
                new LoanQuoteModel
                {
                    Id = "f65e5c7e-de7b-404a-b081-3edd3f629350",
                    Amount = 11000,
                    Term = 5,
                    InterestRate = new Decimal(10),
                    LoanUserId = "591c586e-5d85-4fa0-afd3-f90bbb531895",
                    HasApplied = true
                }
            };

            moduleBuilder.Entity<LoanQuoteModel>().HasData(quotes);
        }
    }
}