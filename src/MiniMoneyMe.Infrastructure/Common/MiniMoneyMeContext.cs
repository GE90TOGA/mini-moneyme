using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MiniMoneyMe.Infrastructure.Model;

namespace MiniMoneyMe.Infrastructure.Common
{
    public class MiniMoneyMeContext : DbContext
    {
        private readonly string _connectionString;

        public DbSet<LoanUserModel> LoanUsers { get; set; }

        public DbSet<LoanQuoteModel> LoanQuotes { get; set; }

        public MiniMoneyMeContext(IOptions<DbSetting> settings)
        {
            _connectionString = settings.Value.ConnectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_connectionString);
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("minimoneyme");
            
            // load to user many-to-one relation
            modelBuilder.Entity<LoanQuoteModel>()
                .HasOne<LoanUserModel>(x => x.LoanUser)
                .WithMany(x => x.Quotes);

            SeedData(modelBuilder);
        }

        protected void SeedData(ModelBuilder modelBuilder)
        {
            SeedDataService.SeedData(modelBuilder);
        }
    }
}