namespace MiniMoneyMe.Infrastructure.Common
{
    public class DbSetting
    {
        public string ConnectionString { get; set; }
    }
}