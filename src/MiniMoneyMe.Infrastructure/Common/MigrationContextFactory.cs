using System;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Options;

namespace MiniMoneyMe.Infrastructure.Common
{
    public class MigrationContextFactory : IDesignTimeDbContextFactory<MiniMoneyMeContext>
    {
        public MiniMoneyMeContext CreateDbContext(string[] args)
        {
            var envConnectionString = Environment.GetEnvironmentVariable("POSTGRES_CONNECTIONSTRING");

            var dbSetting = Options.Create(new DbSetting()
            {
                ConnectionString = !string.IsNullOrWhiteSpace(envConnectionString)
                    ? envConnectionString
                    : "Server=localhost;Username=postgres;Password=postgres;Database=moneyme"
            });

            return new MiniMoneyMeContext(dbSetting);
        }
    }
}