using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MiniMoneyMe.Domain.Exceptions;
using MiniMoneyMe.Domain.QuoteAggregate;
using MiniMoneyMe.Infrastructure.Common;
using MiniMoneyMe.Infrastructure.Model;

namespace MiniMoneyMe.Infrastructure.Repository
{
    public class QuoteRepository : RepositoryBase<Quote>, IQuoteRepository
    {
        public QuoteRepository(MiniMoneyMeContext dbContext) : base(dbContext)
        {
        }

        public override async Task<string> Save(Quote quote)
        {
            LoanQuoteModel loanQuoteModel = null;

            if (!string.IsNullOrWhiteSpace(quote.Id))
            {
                loanQuoteModel = await GetBaseQuery().SingleOrDefaultAsync(q => q.Id == quote.Id);
            }

            if (loanQuoteModel == null)
            {
                // Create new Loan Quote
                loanQuoteModel = new LoanQuoteModel
                {
                    Amount = new Decimal(quote.Amount),
                    Term = quote.Term,
                    InterestRate = new Decimal(quote.InterestRate),
                    HasApplied = false
                };

                // user maybe have been created before, use mobile or email to find possible existing user
                var user = await _dbContext
                    .LoanUsers
                    .SingleOrDefaultAsync(
                        loanUser =>
                            loanUser.Email == quote.Email || loanUser.Mobile == quote.Mobile);

                // Populate existing user's info with latest application info
                if (user != null)
                {
                    user.FirstName = quote.FirstName;
                    user.LastName = quote.LastName;
                    user.Mobile = quote.Mobile;
                    user.Email = quote.Email;
                }

                loanQuoteModel.LoanUser = user ?? new LoanUserModel
                {
                    FirstName = quote.FirstName,
                    LastName = quote.LastName,
                    Mobile = quote.Mobile,
                    Email = quote.Email
                };
            }
            else
            {
                // update a quote
                if (loanQuoteModel.HasApplied)
                {
                    throw new DomainValidationException("Cannot update a quote that has been applied.");
                }

                // Update Existing Loan Quote
                loanQuoteModel.Amount = new Decimal(quote.Amount);
                loanQuoteModel.Term = quote.Term;
                loanQuoteModel.InterestRate = new Decimal(quote.InterestRate);
                loanQuoteModel.LoanUser.FirstName = quote.FirstName;
                loanQuoteModel.LoanUser.LastName = quote.LastName;
                loanQuoteModel.LoanUser.Mobile = quote.Mobile;
                loanQuoteModel.LoanUser.Email = quote.Email;
                loanQuoteModel.HasApplied = quote.HasApplied;
            }

            _dbContext.Update(loanQuoteModel);
            await _dbContext.SaveChangesAsync();

            return loanQuoteModel.Id;
        }

        public override async Task<Quote> GetById(string quoteId)
        {
            var loanQuoteModel = await GetBaseQuery()
                .SingleOrDefaultAsync(quote => quote.Id == quoteId);

            if (loanQuoteModel == null)
            {
                throw new ResourceNotFoundException($"Cannot find quote by quoteId: {quoteId}");
            }

            return new Quote(loanQuoteModel.Id, loanQuoteModel.LoanUser.FirstName,
                loanQuoteModel.LoanUser.LastName, loanQuoteModel.LoanUser.Email, loanQuoteModel.LoanUser.Mobile,
                (double) loanQuoteModel.Amount, loanQuoteModel.Term, (double) loanQuoteModel.InterestRate,
                loanQuoteModel.HasApplied);
        }


        private IQueryable<LoanQuoteModel> GetBaseQuery()
        {
            return _dbContext.LoanQuotes
                .Include(x => x.LoanUser);
        }
    }
}