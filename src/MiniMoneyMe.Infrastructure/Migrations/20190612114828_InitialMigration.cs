﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MiniMoneyMe.Infrastructure.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "minimoneyme");

            migrationBuilder.CreateTable(
                name: "LoanUsers",
                schema: "minimoneyme",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    Mobile = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LoanQuotes",
                schema: "minimoneyme",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    Term = table.Column<int>(nullable: false),
                    InterestRate = table.Column<decimal>(nullable: false),
                    LoanUserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanQuotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_LoanQuotes_LoanUsers_LoanUserId",
                        column: x => x.LoanUserId,
                        principalSchema: "minimoneyme",
                        principalTable: "LoanUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "minimoneyme",
                table: "LoanUsers",
                columns: new[] { "Id", "Email", "FirstName", "LastName", "Mobile" },
                values: new object[,]
                {
                    { "180c5352-2a8b-4941-9728-2790ab5e15af", "johndoeloan@123.com", "John", "Doe", "0414331228" },
                    { "591c586e-5d85-4fa0-afd3-f90bbb531895", "stevejobs@apple.ghost.com", "Steve", "Jobs", "0411332112" }
                });

            migrationBuilder.InsertData(
                schema: "minimoneyme",
                table: "LoanQuotes",
                columns: new[] { "Id", "Amount", "InterestRate", "LoanUserId", "Term" },
                values: new object[,]
                {
                    { "ca24c626-873b-4d27-8e30-bfd26e760b13", 3000m, 3.2m, "180c5352-2a8b-4941-9728-2790ab5e15af", 3 },
                    { "2595e25f-4c80-4dba-9bac-72b554934157", 10000m, 7.2m, "180c5352-2a8b-4941-9728-2790ab5e15af", 4 },
                    { "f65e5c7e-de7b-404a-b081-3edd3f629350", 11000m, 10m, "591c586e-5d85-4fa0-afd3-f90bbb531895", 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_LoanQuotes_LoanUserId",
                schema: "minimoneyme",
                table: "LoanQuotes",
                column: "LoanUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoanQuotes",
                schema: "minimoneyme");

            migrationBuilder.DropTable(
                name: "LoanUsers",
                schema: "minimoneyme");
        }
    }
}
