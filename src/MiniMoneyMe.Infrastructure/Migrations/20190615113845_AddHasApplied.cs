﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MiniMoneyMe.Infrastructure.Migrations
{
    public partial class AddHasApplied : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasApplied",
                schema: "minimoneyme",
                table: "LoanQuotes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                schema: "minimoneyme",
                table: "LoanQuotes",
                keyColumn: "Id",
                keyValue: "2595e25f-4c80-4dba-9bac-72b554934157",
                column: "HasApplied",
                value: true);

            migrationBuilder.UpdateData(
                schema: "minimoneyme",
                table: "LoanQuotes",
                keyColumn: "Id",
                keyValue: "f65e5c7e-de7b-404a-b081-3edd3f629350",
                column: "HasApplied",
                value: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasApplied",
                schema: "minimoneyme",
                table: "LoanQuotes");
        }
    }
}
