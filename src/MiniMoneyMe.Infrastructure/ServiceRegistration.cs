﻿using Microsoft.Extensions.DependencyInjection;
using MiniMoneyMe.Domain.QuoteAggregate;
using MiniMoneyMe.Infrastructure.Common;
using MiniMoneyMe.Infrastructure.Repository;

namespace MiniMoneyMe.Infrastructure
{
    public static class ServiceRegistrations
    {
        public static void RegisterInfrastructure(this IServiceCollection services)
        {
            services.AddEntityFrameworkNpgsql().AddScoped<MiniMoneyMeContext>();
            services.AddTransient<IQuoteRepository, QuoteRepository>();
        }
    }
}
