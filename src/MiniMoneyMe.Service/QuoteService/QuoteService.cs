using System.Threading.Tasks;
using MiniMoneyMe.Domain.Exceptions;
using MiniMoneyMe.Domain.QuoteAggregate;
using MiniMoneyMe.Service.Dto;

namespace MiniMoneyMe.Service.QuoteService
{
    public class QuoteService : IQuoteService
    {
        private readonly IQuoteRepository _quoteRepository;

        public QuoteService(IQuoteRepository quoteRepository)
        {
            _quoteRepository = quoteRepository;
        }

        public async Task<QuoteResponseDto> CreateOrUpdateQuote(QuoteRequestDto quoteRequest)
        {
            var quote = new Quote(quoteRequest.Id, quoteRequest.FirstName,
                quoteRequest.LastName, quoteRequest.Email, quoteRequest.Mobile,
                quoteRequest.Amount, quoteRequest.Term, quoteRequest.InterestRate);

            var id = await _quoteRepository.Save(quote);

            return new QuoteResponseDto
            {
                Id = id,
                FirstName = quote.FirstName,
                LastName = quote.LastName,
                Email = quote.Email,
                Mobile = quote.Mobile,
                Amount = quote.Amount,
                Term = quote.Term,
                InterestRate = quote.InterestRate,
                Repayment = quote.RePayment,
                HasApplied = quote.HasApplied
            };
        }

        public async Task ApplyQuote(string quoteId)
        {
            var quote = await _quoteRepository.GetById(quoteId);
            quote.ApplyQuote();
            await _quoteRepository.Save(quote);
        }

        public async Task<QuoteResponseDto> GetQuoteById(string quoteId)
        {
            var quote = await _quoteRepository.GetById(quoteId);

            return new QuoteResponseDto
            {
                Id = quote.Id,
                FirstName = quote.FirstName,
                LastName = quote.LastName,
                Email = quote.Email,
                Mobile = quote.Mobile,
                Amount = quote.Amount,
                Term = quote.Term,
                InterestRate = quote.InterestRate,
                HasApplied = quote.HasApplied,
                Repayment = quote.RePayment
            };
        }
    }
}