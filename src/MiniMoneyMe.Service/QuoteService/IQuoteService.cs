using System.Threading.Tasks;
using MiniMoneyMe.Service.Dto;

namespace MiniMoneyMe.Service.QuoteService
{
    public interface IQuoteService
    {
        Task<QuoteResponseDto> CreateOrUpdateQuote(QuoteRequestDto quoteRequest);

        Task ApplyQuote(string quoteId);

        Task<QuoteResponseDto> GetQuoteById(string quoteId);
    }
}