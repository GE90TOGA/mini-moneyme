using Microsoft.Extensions.DependencyInjection;
using MiniMoneyMe.Service.QuoteService;

namespace MiniMoneyMe.Service
{
    public static class ServiceRegistration
    {
        public static void RegisterApplicationService(this IServiceCollection services)
        {
            services.AddTransient<IQuoteService, QuoteService.QuoteService>();
        }
    }
}