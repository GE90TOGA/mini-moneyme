namespace MiniMoneyMe.Service.Dto
{
    public class QuoteRequestDto
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Mobile { get; set; }

        public double Amount { get; set; }

        public int Term { get; set; }

        public double InterestRate { get; set; }
    }
}